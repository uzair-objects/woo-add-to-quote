=== Woo Add To Quote ===
Contributors: wpexpertsio
Tags: add quote to cart, quote plugin, cart to quote, email quote, add product to quote, add to quote
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=pay@objects.ws&item_name=Donation For Plugin
Requires at least: 4.7
Tested up to: 5.3.0
Stable tag: 1.4.2
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This Plugin adds woocommerce add to quote functionality and much more...

== Description ==
woo Add To Quote is quote plugin to add woo products to quote, cart products to quote and quote products to cart.
User can add multiple products to quote, move cart products to quote and vice versa.

[youtube https://www.youtube.com/watch?v=RHDAboXC8D0]

= Features: =
- Add multiple products to quote.
- move cart products to quote.
- move quote products to cart.
- email quote to multiple users.
- save quotes to user profile page (if logged in)
- support for localization.

= Admin Features: =
- dashboard to view all the quotes sent.
- option to give admin email (if empty site admin email will be used).
- option to enable/disable quote button on cart page.
- option to enable/disable quote to cart button on quote page.
- option to show/hide add to cart button on product detail page.
- option to show/hide add to cart button from every where in the site.
- option to set text for view quote button.
- option to allow guest user.
- option to empty quote after email.
- option to empty quote after moved quote products to cart.
- option to empty cart after moved cart products to quote.
- option to write messages for notices (successfully email sent/error).
- admin can add html/text before and after the email template.
- admin can change subject of email.

**Interested in contributing to Woo Add To Quote?**	
Head over to the [Woo Add To Quote **GitHub Repository**](https://github.com/wpexpertsio/Woo-Add-To-Quote) to find out how you can pitch in ;)

== Installation ==

1. Go to Plugins > Add New.
2. Under Search, type Woo Add To Quote
3. Find Woo Add To Quote and click Install Now to install it
2. If successful, click Activate Plugin to activate it and you  are ready to go.

== Frequently Asked Questions ==

= How to enable/disable quote button on cart page? =
From back end go to woocommerce setting page, there is a tab named "Quote" you can find this option here. 

= How to enable/disable "add to cart" button from quote page? =
From back end go to woocommerce setting page, there is a tab named "Quote" you can find this option here.  

= Can we add html/text to quote email template? =
Yes. From back end go to woocommerce setting page, there is a tab named "Quote" you can find this option here.


== Screenshots ==

1. Screenshot of Woo Add To Quote quote page.
2. Screenshot of Woo Add To Quote admin quote view page.
3. Screenshot of Woo Add To Quote admin setting page.
4. Screenshot of Woo Quotes on My Account page.
5. Screenshot of Woo Quotes Sending To Given Email.
6. Screenshot of Woo Quotes on Admin Complete Created Quotes.

== Changelog ==

= 1.4.2 =

* Added compatibility for WordPress 5.3
* Added Compatibility WooCommerce 3.8
* Tested with php 7.3
* Fixed Fatal errors
* Fixed Errors of Deprectaed functions
* Fixed Undefined Variable Error
* Fixed Undefined offset[0] Errors

= 1.4.1 =

* Added compatibility for WordPress 5.0.3

= 1.4 =

* Added settings for Build a Quote dynamically.
* Fix the variable product attributes and selected variations.
* Fix the variable product pricing.
* Fix the subtotal and quote total.


= 1.3.7 =
* Fix the variable product price on quote page.

= 1.3.5 =
* Fix the variable product send to admin email .


= 1.3.4 =
* Using Apply Filters developers can customize.

= 1.3.2 =
* Fix the Woocommerce checked all ready exists.
= 1.3 =
* Fix the product title and variation Issue

= 1.2 =
* Checked code compatibility with latest WordPress version 2.9.3 and Woocommerce version 3.3.1

= 1.1 =
* Added functionality to move products from cart to quote

= 1.0 =
* Initial release




